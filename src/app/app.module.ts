import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
//import { AngularFirestore } from 'angularfire2/firestore';
import { environment } from '../environments/environment';

import {
  CardService, CubeService, DeckService,
  DraftService, FirebaseService, UserService
} from '../services';
import {
  ConfigureUserPage, SelectUsersPage, LoginPage,
  SignupPage, MainPage, DraftListPage,
  DraftPacksPage, SearchPage, ConfigurePage,
  TabsPage, CardPage, ManageCubePage,
  ManageDeckPage, CubeSettingsPage
} from '../pages';
import { UserListItem, CardListItem, PackListItem, PersonalItem } from '../components';
import { ManaPipe, SymbolPipe } from '../pipes';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    SignupPage,
    MainPage,
    DraftPacksPage,
    DraftListPage,
    SearchPage,
    ConfigurePage,
    ConfigureUserPage,
    TabsPage,
    CardPage,
    ManageCubePage,
    ManageDeckPage,
    CubeSettingsPage,
    CardListItem,
    PersonalItem,
    SelectUsersPage,
    UserListItem,
    PackListItem,
    ManaPipe,
    SymbolPipe
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    //AngularFirestore,
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      mode: 'md',
      backButtonIcon: 'ios-arrow-back',
      pageTransition: 'ios-transition',
      activator: 'highlight',
      iconMode: 'ios'
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    SignupPage,
    MainPage,
    DraftPacksPage,
    DraftListPage,
    SearchPage,
    ConfigurePage,
    ConfigureUserPage,
    TabsPage,
    CardPage,
    ManageCubePage,
    ManageDeckPage,
    CubeSettingsPage,
    CardListItem,
    PersonalItem,
    SelectUsersPage,
    UserListItem,
    PackListItem
  ],
  providers: [
    CardService,
    CubeService,
    DeckService,
    DraftService,
    FirebaseService,
    UserService,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
