import { Component, ViewChild } from '@angular/core';
import { LoadingController, Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { UserService } from '../services';
import { LoginPage, TabsPage } from '../pages';


@Component({
  template: '<ion-nav #rootNav></ion-nav>'
})
export class MyApp {
  @ViewChild('rootNav') rootNav: Nav;

  constructor(
    public userSvc: UserService,
    public loadingCtrl: LoadingController,
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen
  ) {
    platform.ready().then(() => {
      statusBar.hide();

      let loading = this.loadingCtrl.create({ content: 'loading...' });
      loading.present();

      this.userSvc.auth$.subscribe(isAuthed => {
        splashScreen.hide();

        this.rootNav.setRoot(
          isAuthed ? TabsPage : LoginPage, { loading }
        );
      });
    });
  }

}
