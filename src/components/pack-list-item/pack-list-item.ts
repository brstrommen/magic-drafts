import {
  Input,
  Component,
  Output,
  OnInit,
  EventEmitter
} from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DraftService } from '../../services';


@Component({
  selector: 'md-pack-list-item',
  templateUrl: 'pack-list-item.html'
})
export class PackListItem implements OnInit {
  @Input('disable') disable: boolean;
  @Input('draftId') draftId: string;
  @Input('packId') packId: string;
  @Output() draftCard: EventEmitter<any> = new EventEmitter<Object>();
  public packCards: Observable<any>;
  public draftedCards: Observable<any>;

  constructor(
    public draftSvc: DraftService
  ) { }

  ngOnInit() {
    this.packCards = this.draftSvc.getQueueCards(this.draftId, this.packId);
    this.draftedCards = this.draftSvc.getDecklist(this.draftId);
  }

  public addCard(cardId) {
    this.draftCard.emit({
      cardId, packId: this.packId
    });
  }

}
