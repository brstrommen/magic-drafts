import {
  Input,
  Component,
  Output,
  OnInit,
  EventEmitter
} from '@angular/core';
import { ModalController } from 'ionic-angular';

import { CardService } from '../../services';
import { CardPage } from '../../pages';


@Component({
  selector: 'md-card-list-item',
  templateUrl: 'card-list-item.html'
})
export class CardListItem implements OnInit {
  public card: any;
  @Input('cardId') cardId: string;
  @Input('context') context: string;
  @Input('disable') disable: boolean;
  @Output() addCard: EventEmitter<any> = new EventEmitter<Object>();
  @Output() removeCard: EventEmitter<any> = new EventEmitter<Object>();
  @Output() moveUp: EventEmitter<any> = new EventEmitter<Object>();
  @Output() moveDown: EventEmitter<any> = new EventEmitter<Object>();

  constructor(
    public modalCtrl: ModalController,
    public cardSvc: CardService
  ) { }

  ngOnInit() {
    this.cardSvc.getCard(this.cardId).subscribe(card => this.card = card);
  }

  public viewDetails() {
    this.modalCtrl.create(CardPage, { cardId: this.cardId }).present();
  }

}
