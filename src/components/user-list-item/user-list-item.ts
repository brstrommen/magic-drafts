import {
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter
} from '@angular/core';
import { Observable } from 'rxjs';

import { UserService } from '../../services';


@Component({
  selector: 'md-user-list-item',
  templateUrl: 'user-list-item.html'
})
export class UserListItem implements OnInit {
  @Input('userId') userId: string;
  @Input('context') context: string;
  @Input('selected') selected: boolean;
  @Input('canSelect') canSelect: boolean;
  @Output() selectUser: EventEmitter<any> = new EventEmitter<Object>();

  public user: Observable<any>;

  constructor(
    public userSvc: UserService
  ) { }

  ngOnInit(): void {
    this.user = this.userSvc.getUserProfile(this.userId);
  }

}
