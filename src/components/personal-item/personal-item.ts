import {
  Input,
  Component,
  OnInit
} from '@angular/core';
import { NavController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';

import { ManageCubePage, ManageDeckPage } from '../../pages';
import { CubeService, DeckService } from '../../services';


@Component({
  selector: 'md-personal-item',
  templateUrl: 'personal-item.html'
})
export class PersonalItem implements OnInit {
  @Input('itemType') itemType: string;
  @Input('itemId') itemId: any;
  public item: Observable<any>

  constructor(
    public navCtrl: NavController,
    public cubeSvc: CubeService,
    public deckSvc: DeckService
  ) { }

  ngOnInit() {
    if (this.itemType === 'cube') {
      this.item = this.cubeSvc.getCube(this.itemId);
    } else if (this.itemType === 'deck') {
      this.item = this.deckSvc.getDeck(this.itemId);
    }
  }

  public manage(item) {
    if (this.itemType === 'cube') {
      this.navCtrl.push(ManageCubePage, { cubeId: this.itemId });
    } else if (this.itemType === 'deck') {
      this.navCtrl.push(ManageDeckPage, { deckId: this.itemId });
    } else {
      //
    }
  }

}
