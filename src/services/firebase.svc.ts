import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFireDatabase, ChildEvent } from 'angularfire2/database';


@Injectable()
export class FirebaseService {

  constructor ( public afDb: AngularFireDatabase ) { }

  /**
   * @description Return the value of an object
   * @public
   * @param {string} path
   * @returns {Observable<any>}
   */
  public objectValue(path: string): Observable<any> {
    if (!this.checkPath(path))
      return Observable.throw('bad path in object value');
    else return this.afDb.object(path).valueChanges()
      .catch(err => this.handlePermissionErrors(err));
  }

  /**
   * @description Return the data snapshot of an object
   * @public
   * @param {string} path
   * @returns {Observable<any>}
   */
  public objectSnapshot(path: string): Observable<any> {
    if (!this.checkPath(path))
      return Observable.throw('bad path in object snapshot');
    else return this.afDb.object(path).snapshotChanges()
      .catch(err => this.handlePermissionErrors(err));
  }

  /**
   * @description Return the value of a list
   * @public
   * @param {string} path
   * @returns {Observable<any>}
   */
  public listValue(path: string): Observable<any> {
    if (!this.checkPath(path))
      return Observable.throw('bad path in list value');
    else return this.afDb.list(path).valueChanges()
      .catch(err => this.handlePermissionErrors(err));
  }

  /**
   * @description Return the data snapshot of a list
   * @public
   * @param {string} path
   * @param {any} refFunction
   * @returns {Observable<any>}
   */
  public listSnapshot(
    path: string,
    refFunction: any = ref => ref,
    events: ChildEvent[] = [ 'child_added', 'child_removed', 'child_changed', 'child_moved' ]
  ): Observable<any> {
    if (!this.checkPath(path))
      return Observable.throw('bad path in list snapshot');
    else return this.afDb.list(path, refFunction).snapshotChanges(events)
      .catch(err => this.handlePermissionErrors(err));
  }

  /**
   * @description Push data to a list
   * @public
   * @param {string} path
   * @param {Object} data
   * @returns {Observable<any>}
   */
  public push(path: string, data: Object): Observable<any> {
    if (!this.checkPath(path))
      return Observable.throw('bad path in push');
    else return Observable.fromPromise(
      this.afDb.list(path).push(data)
    );
  }

  /**
   * @description Update the value of an object
   * @public
   * @param {string} path
   * @param {Object} data
   * @returns {Observable<any>}
   */
  public update(path: string, data: Object): Observable<any> {
    if (!this.checkPath(path))
      return Observable.throw('bad path in update');
    else return Observable.fromPromise(
      this.afDb.object(path).update(data)
    );
  }

  /**
   * @description Set the value of an object
   * @public
   * @param {string} path
   * @param {any} data
   * @returns {Observable<any>}
   */
  public set(path: string, data: any): Observable<any> {
    if (!this.checkPath(path))
      return Observable.throw('bad path in set');
    else return Observable.fromPromise(
      this.afDb.object(path).set(data)
    );
  }


  /**
   * @description Verify an intended path is valid
   * @private
   * @param {string} path
   * @returns {boolean}
   */
  private checkPath(ref): boolean {
    const path = ref.split(`/`);

    if ((path.indexOf('null') !== -1) || (path.indexOf('undefined') !== -1)) {
      console.log(`${ref} is bad path`);
      return false;
    } else {
      return true;
    }
  }

  /**
   * @description Handle specific data access errors
   * @private
   * @param {any} err
   * @returns {Observable<any>}
   */
  private handlePermissionErrors(err: any): Observable<any> {
    if ( err.toString() && err.toString().indexOf('permission_denied') > -1 ) {
      console.log('ERROR', err.toString());
      return Observable.of(null);
    } // end if

    return Observable.throw(err);
  }

}
