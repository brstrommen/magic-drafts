import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { filter, pull, random } from 'lodash';

import { CubeService } from './cube.svc';
import { DeckService } from './deck.svc';
import { FirebaseService } from './firebase.svc';
import { UserService } from './user.svc';

const PACKS_IN_DRAFT = 3;
const CARDS_IN_PACK = 15;


@Injectable()
export class DraftService {
  public auth$: Observable<boolean>;
  public uid: string;

  constructor (
    public cubeSvc: CubeService,
    public deckSvc: DeckService,
    public fbSvc: FirebaseService,
    public userSvc: UserService
  ) {
    console.log('Constructing DraftService');
    this.userSvc.uid$.subscribe(uid => this.uid = uid);
  }

  public getDecklist(draftId): Observable<any> {
    return this.deckSvc.getDecklist(`${this.uid}-${draftId}`);
  }

  public getMyDrafts(): Observable<any> {
    return this.userSvc.uid$.switchMap(uid =>
      this.fbSvc.listSnapshot(`/users/${uid}/drafts`)
    );
  } //getMyDrafts

  public getDraft(draftId): Observable<any> {
    return this.fbSvc.objectValue(`/drafts/${draftId}`);
  } //getDraft

  public getDraftQueue(draftId): Observable<any> {
    return this.fbSvc.listSnapshot(`drafts/${draftId}/packs`,
      ref => ref.orderByChild('uid').equalTo(this.uid)
    );
  } //getDraftQueue

  public getQueueCards(draftId, packId): Observable<any> {
    return this.fbSvc.listSnapshot(`drafts/${draftId}/packs/${packId}/cards`)
      .map(cards => filter(cards, card => card.payload.val() === true));
  } //getQueueCards

  public startCubeDraft (
    users: string[],
    cubeId: string
  ): Observable<string> {
    return this.cubeSvc.getCube(cubeId).take(1)
      .do(() => users.push(this.uid)).switchMap(cubeData =>
        this.fbSvc.push(`/drafts`, {
          users: users,
          owner: this.uid,
          cubeId: cubeId,
          time: Date.now(),
          name: `${cubeData.name} Cube Draft`
        }).switchMap(draft =>
          this.createCubePacks(cubeId, draft.key, users)
            .map(packsCreated => draft.key)
        )
    ).take(1);
  } //startCubeDraft

  private createCubePacks(
    cubeId: string,
    draftId: string,
    players: string[]
  ): Observable<string> {
    return this.fbSvc.objectValue(`/cubes/${cubeId}/cardlist`).take(1)
      .switchMap(cards => {
        const cardList = Object.keys(cards);
        const packs = [];

        for(let i = 0; i < PACKS_IN_DRAFT; i++) {
          for(let j = 0; j < players.length; j++) {
            const myPack = {
              cards: { },
              packNumber: i + 1,
              owner: players[j]
            };

            for(let k = 0; k < CARDS_IN_PACK; k++) {
              const cardId = cardList[random(0, cardList.length - 1)];
              pull(cardList, cardId);

              myPack.cards[cardId] = true;
            }

            packs.push(this.fbSvc.push(`/drafts/${draftId}/packs`, myPack));
          }
        }

      return Observable.combineLatest(packs);
    }).switchMap(packsCreated =>{
      const openPacks = [ ];
      players.forEach(player =>
        openPacks.push(
          this.openPack(draftId, player)
        )
      );
      return Observable.combineLatest(openPacks)
        .map(packsOpened => draftId);
    }).take(1);
  } //createCubePacks

  private getMyPacks(draftId, uid: string = this.uid): Observable<any> {
    return this.fbSvc.listSnapshot(`drafts/${draftId}/packs`,
      ref => ref.orderByChild('owner').equalTo(uid)
    ).map(packs => filter(packs, pack => !pack.payload.val().opened));
  } //getMyPacks

  private openPack(draftId, uid: string = this.uid): Observable<any> {
    return this.getMyPacks(draftId, uid).take(1)
      .map(packs => packs[0].key)
      .switchMap(packId =>
        this.fbSvc.update(`drafts/${draftId}/packs/${packId}`, {
          opened: true, uid
        })
      ).take(1);
  } //openPack

  public draftCard(draftId, packId, cardId): Observable<any> {
    return Observable.combineLatest(
      this.fbSvc.set(`decks/${this.uid}-${draftId}/cards/${cardId}`, true),
      this.fbSvc.set(`drafts/${draftId}/packs/${packId}/cards/${cardId}`, this.uid)
    ).switchMap(() =>
      Observable.combineLatest(
        this.getNextUser(draftId, packId),
        this.getQueueCards(draftId, packId)
      ).take(1).switchMap(([nextUser, cards]) => {
        if (cards.length !== 0) {
          return this.fbSvc.set(`drafts/${draftId}/packs/${packId}/uid`, nextUser)
            .map(res => true);
        } else {
          return Observable.combineLatest(
            this.fbSvc.set(`drafts/${draftId}/packs/${packId}/uid`, null),
            this.openPack(draftId)
          ).map(res => true);
        }
      })
    ).take(1);
  } //draftCard

  private getNextUser(draftId, packId): Observable<any> {
    return Observable.combineLatest(
      this.fbSvc.listValue(`drafts/${draftId}/users`),
      this.fbSvc.objectValue(`drafts/${draftId}/packs/${packId}/packNumber`)
    ).take(1).switchMap(([users, packNumber]) => {
      let userIndex = users.indexOf(this.uid);

      if (packNumber === 2) { //pass right
        userIndex -= 1;
        if (userIndex < 0) { userIndex = users.length - 1}
      } else { //pass left
        userIndex += 1;
        if (userIndex === users.length) { userIndex = 0; }
      }

      return Observable.of(users[userIndex]);
    }).take(1);
  } //getNextUser

} //DraftService
