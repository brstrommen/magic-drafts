import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { difference, keys, map } from 'lodash';

import { FirebaseService } from './firebase.svc';
import { UserService } from './user.svc';


/**
 * @class AnalyticsService
 * @module CoreModule
 */
@Injectable()
export class CubeService {
  constructor (
    public fbSvc: FirebaseService,
    public userSvc: UserService
  ) {
    console.log('Constructing CubeService');
  }

  public getCube(cubeId: string): Observable<any> {
    return this.fbSvc.objectValue(`/cubes/${cubeId}`);
  }

  public getCubeList(): Observable<any> {
    return this.userSvc.uid$.switchMap(uid =>
      this.fbSvc.objectValue(`/users/${uid}/cubes`)
    ).map(cubeList => keys(cubeList)
    ).switchMap(cubeList => Observable.combineLatest(
      map(cubeList, cube =>
        Observable.combineLatest(
          this.getCube(cube).pluck('name'),
          Observable.of(cube)
        ).map(([name, key]) =>
          ({ name, key })
        )
      )
    ));
  }

  public getAllCubeData(): Observable<any> {
    return this.fbSvc.objectValue(`/cubes`);
  }

  public createCube(name: string): Observable<any> {
    return this.userSvc.uid$.switchMap(uid =>
      this.fbSvc.push(`/cubes`, {
        name: name,
        owner: uid
      })
    ).take(1);
  }

  public addCardToCube(cubeId: string, cardId: string): Observable<any> {
    return this.fbSvc.set(`/cubes/${cubeId}/cardlist/${cardId}`, true);
  }

  public removeCardFromCube(cubeId: string, cardId: string): Observable<any> {
    return this.fbSvc.set(`/cubes/${cubeId}/cardlist/${cardId}`, null);
  }

  private removeUserFromCube(cubeId, userId) {
    return Observable.combineLatest(
      this.fbSvc.set(`/users/${userId}/cubes/${cubeId}`, null),
      this.fbSvc.set(`/cubes/${cubeId}/collaborators/${userId}`, null)
    ).take(1);
  }

  private addUserToCube(cubeId, userId) {
    return this.fbSvc.set(`/cubes/${cubeId}/collaborators/${userId}`, true);
  }

  public modifyCollaborators(cubeId, newUsers): Observable<any> {
    return this.fbSvc.objectValue(`/cubes/${cubeId}`)
      .pluck('collaborators').take(1).switchMap(collabs => {
        const currentUsers = keys(collabs);
        const modifiedUsers = [ ];

        //addedUsers
        difference(newUsers, currentUsers).forEach(addedUser =>
          modifiedUsers.push(this.addUserToCube(cubeId, addedUser))
        );

        //removedUsers
        difference(currentUsers, newUsers).forEach(removedUser =>
          modifiedUsers.push(this.removeUserFromCube(cubeId, removedUser))
        );

        return Observable.combineLatest(modifiedUsers);
      }).take(1);
  }

}
