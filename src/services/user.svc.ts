import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFireAuth } from 'angularfire2/auth';

import { FirebaseService } from './firebase.svc';


@Injectable()
export class UserService {
  public auth$: Observable<boolean>;
  public uid$: Observable<string>;

  constructor (
    public afAuth: AngularFireAuth,
    public fbSvc: FirebaseService
  ) {
    console.log('Constructing UserService');
    this.auth$ = this.afAuth.authState.map(user => !!user);
    this.uid$ = this.afAuth.authState.filter(s => !!s).map(user => user.uid);
  }

  public createAccount(
    email: string,
    username: string,
    password: string
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      this.afAuth.auth.createUserWithEmailAndPassword(email, password)
        .then(res => {
          this.fbSvc.update(`users/${res.uid}`, {
            username, email
          });
          resolve();
        }).catch(err => reject(err));
    });
  } //createAccount

  public signIn(email: string, password: string): Promise<any> {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password);
  } //signIn

  public signOut(): void {
    this.afAuth.auth.signOut();
  } //signOut

  public getAllUsers(): Observable<any> {
    return this.fbSvc.listSnapshot(`/users`, ref => ref.orderByChild('username'));
  } //getAllUsers

  public getUserProfile(uid: string): Observable<any> {
    return this.fbSvc.objectValue(`/users/${uid}`);
  } //getUserProfile

  public changeUsername(newName: string): Observable<any> {
    return this.uid$.switchMap(uid =>
      this.fbSvc.set(`/users/${uid}/username`, newName)
    );
  }

} //UserService
