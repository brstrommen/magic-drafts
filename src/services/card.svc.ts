import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { FirebaseService } from './firebase.svc';


/**
 * @class AnalyticsService
 * @module CoreModule
 */
@Injectable()
export class CardService {

  constructor (
    public fbSvc: FirebaseService
  ) {
    console.log('Constructing CardService');
  }

  public getCard(cardId): Observable<any> {
    return this.fbSvc.objectValue(`/cards/${cardId}`).take(1);
  }

  public getCardList(name, num): Observable<any> {
    return this.fbSvc.listSnapshot('/cards',
      ref => ref.orderByChild('name').startAt(name).limitToFirst(num)
    );
  }

}
