import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { keys, pickBy } from 'lodash';

import { UserService } from './user.svc';
import { FirebaseService } from './firebase.svc';


@Injectable()
export class DeckService {
  private uid: string;

  constructor (
    public fbSvc: FirebaseService,
    public userSvc: UserService
  ) {
    console.log('Constructing DeckService');
    this.userSvc.uid$.subscribe(uid => this.uid = uid);
  }

  public getDeck(deckId): Observable<any> {
    return this.fbSvc.objectValue(`decks/${deckId}`)
  } //getDeck

  public getDecklist(deckId): Observable<any> {
    return this.fbSvc.objectValue(`decks/${deckId}/cards`).map(keys);
  } //getDecklist

  public getMainboard(deckId): Observable<any> {
    return this.fbSvc.objectValue(`decks/${deckId}/cards`)
      .map(decklist => keys(
        pickBy(decklist, status => status === 'mainboard'))
      );
  } //getMainboard

  public getSideboard(deckId): Observable<any> {
    return this.fbSvc.objectValue(`decks/${deckId}/cards`)
      .map(decklist => keys(
        pickBy(decklist, status => status === 'sideboard'))
      );
  } //getSideboard

  public listDecks(userId: string = this.uid): Observable<any> {
    return this.fbSvc.listSnapshot(`users/${userId}/decks`);
  } //getDeckList

  public mainboard(deckId, cardId): Observable<any> {
    return this.fbSvc.set(`decks/${deckId}/cards/${cardId}`, 'mainboard');
  } //mainboard

  public sideboard(deckId, cardId): Observable<any> {
    return this.fbSvc.set(`decks/${deckId}/cards/${cardId}`, 'sideboard');
  } //sideboard

} //DeckService
