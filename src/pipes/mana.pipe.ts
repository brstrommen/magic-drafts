import { Pipe, PipeTransform } from '@angular/core';

/**
 * @description returns Observable for use with `async` pipe
 * @module SharedModule
 */
@Pipe({
  name: 'mana'
})
export class ManaPipe implements PipeTransform {

  constructor( ) { }

  transform(mana: string) {
    if (!mana) { return []; }
    return mana
      .replace(/[/]/g, '')
      .split(/([{]\w+[}]){1}/g)
      .filter(x => !!x);
  }

}
