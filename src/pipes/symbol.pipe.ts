import { Pipe, PipeTransform } from '@angular/core';

/**
 * @description returns Observable for use with `async` pipe
 * @module SharedModule
 */
@Pipe({
  name: 'symbol'
})
export class SymbolPipe implements PipeTransform {

  constructor( ) { }

  transform(symbol: string) {
    return `./assets/imgs/${symbol.replace(/[{/}]/g, '')}.svg`;
  }

}
