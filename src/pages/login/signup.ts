import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, NavController } from 'ionic-angular';

import { TabsPage } from '../';
import { UserService } from '../../services';


@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  public signupForm: FormGroup;

  constructor(
    public formBuilder: FormBuilder,
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public userSvc: UserService
  ) {

    this.signupForm = this.formBuilder.group({
      email: [ null, Validators.compose([
        Validators.required,
        Validators.email
      ]) ],
      username: [ null, Validators.compose([
        Validators.required,
        Validators.minLength(4)
      ]) ],
      password: [ null, Validators.compose([
        Validators.required,
        Validators.minLength(6)
      ]) ],
      confirm: [ '', null ]
    });

  } //constructor

  public createAccount(formData) {
    this.userSvc.createAccount(formData.email, formData.username, formData.password)
      .then(() => this.navCtrl.push(TabsPage))
      .catch(err => console.log(err));
  }

} //SignupPage
