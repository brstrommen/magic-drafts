import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, NavController, NavParams } from 'ionic-angular';

import { SignupPage } from './signup';
import { TabsPage } from '../';
import { UserService } from '../../services';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  public loginForm: FormGroup;

  constructor(
    public formBuilder: FormBuilder,
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public userSvc: UserService
  ) {
    if (this.navParams.get('loading')) {
      this.navParams.get('loading').dismiss();
    }

    this.loginForm = this.formBuilder.group({
      email: [ null, Validators.compose([
        Validators.required, Validators.email
      ]) ],
      password: [ null, Validators.required ]
    });
  } //constructor

  public signIn(email, password) {
    this.userSvc.signIn(email, password)
      .then(() => this.navCtrl.push(TabsPage))
      .catch(err => console.log(err));
  }

  public goToCreateAccount() {
    this.navCtrl.push( SignupPage );
  }

} //LoginPage
