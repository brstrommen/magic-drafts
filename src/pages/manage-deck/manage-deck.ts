import { Component } from '@angular/core';
import {
  ModalController,
  NavController,
  NavParams,
  ToastController
} from 'ionic-angular';
import { Observable } from 'rxjs';

import { CardService, DeckService } from '../../services';


@Component({
  selector: 'page-manage-deck',
  templateUrl: 'manage-deck.html'
})
export class ManageDeckPage {
  private deckId: string;
  public deck: Observable<any>;
  public decklist: Observable<any>;
  public mainboard: Observable<any>;
  public sideboard: Observable<any>;

  public showMainboard: boolean = true;
  public showSideboard: boolean = true;
  public showDecklist: boolean = true; //TODO: Switch back to false

  constructor(
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public cardSvc: CardService,
    public deckSvc: DeckService
  ) {
    this.deckId = this.navParams.get('deckId');
    this.deck = this.deckSvc.getDeck(this.deckId);
    this.decklist = this.deckSvc.getDecklist(this.deckId);
    this.mainboard = this.deckSvc.getMainboard(this.deckId);
    this.sideboard = this.deckSvc.getSideboard(this.deckId);
    //this.showDecklist = !!(this.navParams.get('showDecklist') === true);
  }

  public doMainboard(cardId) {
    this.deckSvc.mainboard(this.deckId, cardId).subscribe();
  } //moveUp

  public doSideboard(cardId) {
    this.deckSvc.sideboard(this.deckId, cardId).subscribe();
  } //moveDown

  public manageDeck() {
    console.log('Manage Deck');
  }

}
