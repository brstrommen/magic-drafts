import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Observable } from 'rxjs';

import { ConfigureUserPage } from '../configure/configure-user';
import { CubeService, DeckService } from '../../services';

@Component({
  selector: 'page-configure',
  templateUrl: 'configure.html'
})
export class ConfigurePage {
  public cubes$: Observable<any>;
  public decks$: Observable<any>;

  constructor(
    public navCtrl: NavController,
    public cubeSvc: CubeService,
    public deckSvc: DeckService
  ) {
    this.cubes$ = this.cubeSvc.getCubeList();
    this.decks$ = this.deckSvc.listDecks();
  }

  public mySettings() {
    this.navCtrl.push(ConfigureUserPage);
  }

}
