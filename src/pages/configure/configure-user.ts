import { Component } from '@angular/core';
import { AlertController, ToastController } from 'ionic-angular';
import { Observable } from 'rxjs';

import { UserService } from '../../services';

@Component({
  selector: 'page-configure-user',
  templateUrl: 'configure-user.html'
})
export class ConfigureUserPage {
  public user: Observable<any>;

  constructor(
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public userSvc: UserService
  ) {
    this.user = this.userSvc.uid$.switchMap(uid => this.userSvc.getUserProfile(uid));
  }

  public changeName() {
    this.alertCtrl.create({
      title: 'Enter New Username',
      buttons: [ {
        text: 'Cancel',
        role: 'cancel',
        cssClass: 'subdued'
      }, {
        text: 'Confirm',
        handler: (e) =>  {
          this.userSvc.changeUsername(e.username)
            .subscribe(res =>
              this.toastCtrl.create({
                message: 'Username changed successfully!',
                duration: 1500,
                position: 'top'
              }).present()
            );
          }
      } ],
      inputs: [ {
        type: 'text',
        name: 'username',
        placeholder: 'username'
      }]
    }).present();
  }

  public signOut() {
    this.userSvc.signOut();
  }

}
