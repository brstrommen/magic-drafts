import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { CardService } from '../../services';


@Component({
  selector: 'page-card',
  templateUrl: 'card.html'
})
export class CardPage {
  public card;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public cardSvc: CardService
  ) { }

  ngOnInit() {
    this.cardSvc.getCard(this.navParams.get('cardId'))
      .subscribe(card => this.card = card);
  }
}
