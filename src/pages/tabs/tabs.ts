import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';

import { MainPage, SearchPage, ConfigurePage } from '../';


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = MainPage;
  tab2Root = SearchPage;
  tab3Root = ConfigurePage;

  constructor(
    public navParams: NavParams
  ) {
    if (this.navParams.get('loading')) {
      this.navParams.get('loading').dismiss();
    }
  }
}
