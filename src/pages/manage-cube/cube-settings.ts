import { Component } from '@angular/core';
import {
  ModalController,
  NavController,
  NavParams,
  ToastController
} from 'ionic-angular';
import { Observable } from 'rxjs';
import { keys } from 'lodash';

import { SelectUsersPage } from '../';
import { CubeService } from '../../services/cube.svc';


@Component({
  selector: 'page-cube-settings',
  templateUrl: 'cube-settings.html'
})
export class CubeSettingsPage {
  public cubeId: string;
  public cube: Observable<any>;
  public cards: string[];

  constructor(
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public cubeSvc: CubeService
  ) {
    this.cubeId = this.navParams.get('cubeId');
    this.cubeSvc.getCube(this.cubeId).subscribe(cube => {
      this.cube = cube;
      this.cards = keys(this.cube['list']);
    });
  }

  public manageCollaborators() {
    let currentCollaborators = keys(this.cube['collaborators']);
    let selectUsers = this.modalCtrl.create(SelectUsersPage, {
      preselected: currentCollaborators,
      omitCurrentUser: true
    });
    selectUsers.present();
    selectUsers.onDidDismiss(selection => {
      if (selection === null || selection === undefined) { return; }
      else {
        this.cubeSvc.modifyCollaborators(this.cubeId, selection).subscribe();
      }
    });
  } //addCollaborator

  public exportCube() { } //exportCube

}
