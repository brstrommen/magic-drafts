import { Component } from '@angular/core';
import {
  ModalController,
  NavController,
  NavParams,
  ToastController
} from 'ionic-angular';
import { Observable } from 'rxjs';
import { keys } from 'lodash';

import { CubeSettingsPage } from '../';
import { CardService, CubeService } from '../../services';


@Component({
  selector: 'page-manage-cube',
  templateUrl: 'manage-cube.html'
})
export class ManageCubePage {
  public cubeId: string;
  public cube: Observable<any>;
  public cards: string[];

  constructor(
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public cardSvc: CardService,
    public cubeSvc: CubeService
  ) {
    this.cubeId = this.navParams.get('cubeId');
    this.cubeSvc.getCube(this.cubeId).subscribe(cube => {
      this.cube = cube;
      this.cards = keys(this.cube['list']);
    });
  }

  public moveUp(cardId) {
    console.log(cardId);
  } //moveUp

  public moveDown(cardId) {
    console.log(cardId);
  } //moveDown

  public removeCard(cardId) {
    this.cardSvc.getCard(cardId).subscribe(card =>
      this.cubeSvc.removeCardFromCube(this.navParams.get('cubeId'), cardId)
        .subscribe(() => this.toastCtrl.create({
          message: `You removed ${card.name}.`,
          duration: 3000,
          position: 'top'
        }).present()
      )
    );
  } //removeCard

  public manageCube() {
    this.navCtrl.push(CubeSettingsPage, { cubeId: this.cubeId });
  } //manageCube

}
