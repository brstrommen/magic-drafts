import { Component } from '@angular/core';
import { AlertController, ToastController } from 'ionic-angular';
import { Observable } from 'rxjs';
import { last, map } from 'lodash';

import { CardService, CubeService, UserService } from '../../services';


@Component({
  selector: 'page-search',
  templateUrl: 'search.html'
})
export class SearchPage {
  private PAGE_OFFSET: number = 25;
  private cubes$: Observable<any>;
  public isMorePages: boolean = true;
  public pages: any[] = [];
  public searchText: string = '';

  constructor(
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public cardSvc: CardService,
    public cubeSvc: CubeService,
    public userSvc: UserService
  ) {
    this.doSearch();
    this.cubes$ = this.cubeSvc.getCubeList();
  }

  public doSearch() {
    this.isMorePages = true;
    this.pages = [ ];
    this.pushPage(this.searchText);
  } //doSearch

  public pushPage(name) {
    this.cardSvc.getCardList(name, this.PAGE_OFFSET)
      .subscribe(newPage => {
        this.pages.push(newPage);
        if (newPage.length < this.PAGE_OFFSET) {
          this.isMorePages = false;
        }
      });
  } //pushPage

  public doInfinite(event) {
    this.pushPage(
      last(last(this.pages)).payload.val().name + 'a'
    );

    setTimeout(() => event.complete(), 750);
  } //doInfinite

  public addCardToCube(card) {
    this.cubes$.take(1).subscribe(cubes => {
      if (!cubes.length) {
        return this.toastCtrl.create({
          message: 'You need to create a Cube first!',
          position: 'middle',
          duration: 2250
        }).present();
      }

      this.alertCtrl.create({
        title: card.payload.val().name,
        subTitle: 'Add card to cube?',
        inputs: map(cubes, cube => ({
          type: 'radio',
          label: cube.name,
          value: cube.key
        })), buttons: [{
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'subdued'
        }, {
          text: 'Add',
          role: 'cancel',
          handler: (cubeId) => {
            this.cubeSvc.addCardToCube(cubeId, card.key).subscribe(
              () => this.toastCtrl.create({
                message: `${card.payload.val().name} was added to your cube!`,
                duration: 1500,
                position: 'top'
              }).present()
            );
          }
        }]
      }).present();

    });
  }

}
