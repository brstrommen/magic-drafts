import { Component } from '@angular/core';
import { AlertController, NavController, NavParams } from 'ionic-angular';
import { Observable, ReplaySubject } from 'rxjs';

import { ManageDeckPage } from '../manage-deck/manage-deck';
import {
  CardService,
  DeckService,
  DraftService,
  UserService
} from '../../services/';


@Component({
  selector: 'page-draft-packs',
  templateUrl: 'draft-packs.html'
})
export class DraftPacksPage {
  public draftId: string;
  public draftName: Observable<any>
  public myPacks: Observable<any>;
  public myDeck: Observable<any>;
  public myQueue: Observable<any>;
  public queueContents: Observable<any>;
  public waiting: boolean = false;
  public complete$: ReplaySubject<boolean> = new ReplaySubject(1);

  constructor(
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public cardSvc: CardService,
    public draftSvc: DraftService,
    public deckSvc: DeckService,
    public userSvc: UserService
  ) {
    this.draftId = this.navParams.get('draftId');
    this.draftName = this.draftSvc.getDraft(this.draftId).pluck('name').take(1);
    this.myDeck = this.draftSvc.getDecklist(this.draftId);
    this.myQueue = this.draftSvc.getDraftQueue(this.draftId);

    Observable.combineLatest(
      this.userSvc.uid$, this.myDeck
    ).takeUntil(this.complete$).subscribe(([uid, deck]) => {
      if(deck.length === 45) {
        this.complete$.next(true);
        this.navCtrl.push(ManageDeckPage, {
          deckId: `${uid}-${this.draftId}`,
          showDecklist: true
        });
      }
    });
  } //constructor

  ionViewWillLeave(): void {
    this.complete$.next(true);
    this.complete$.complete();
  }

  public pickCard(IDs) {
    this.waiting = true;
    this.cardSvc.getCard(IDs.cardId).subscribe(card => {
      this.alertCtrl.create({
        title: 'Confirm Selection',
        subTitle: `Are you sure you want to draft ${card.name}?`,
        buttons: [{
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'subdued',
          handler: () => {
            this.waiting = false;
          }
        }, {
          text: 'Select',
          role: 'confirm',
          handler: () => {
            this.draftSvc.draftCard(
              this.draftId,
              IDs.packId,
              IDs.cardId
            ).subscribe(() => this.waiting = false);
          }
        }]
      }).present();
    });
  } //pickCard

} //DraftPacksPage
