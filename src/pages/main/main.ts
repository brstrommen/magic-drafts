import { Component } from '@angular/core';
import { AlertController, ModalController, NavController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';

import { DraftPacksPage, DraftListPage } from '../';
import { SelectUsersPage } from '../select-users/select-users';
import { CubeService, DraftService } from '../../services/';


@Component({
  selector: 'page-main',
  templateUrl: 'main.html'
})
export class MainPage {
  public cubeData: Observable<any>;
  public myCubes: Observable<any>;
  public myDrafts: Observable<any>;

  constructor(
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public cubeSvc: CubeService,
    public draftSvc: DraftService
  ) {
    this.myCubes = this.cubeSvc.getCubeList();
    this.myDrafts = this.draftSvc.getMyDrafts();
  } //constructor

  public startNewDraft() {
    //
  }

  //TODO: do this, but less gross
  public startCubeDraft() {
    Observable.combineLatest(
      this.myCubes, this.cubeSvc.getAllCubeData()
    ).take(1).subscribe(([cubeList, cubeData]) => {
      const radioOpts = [];
      cubeList.forEach(cube => {
        radioOpts.push({
          type: 'radio',
          label: cubeData[cube.key].name,
          value: cube.key
        });
      });

      this.alertCtrl.create({
        title: 'Select Cube',
        inputs: radioOpts,
        buttons: [{
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'subdued'
        }, {
          text: 'Select',
          role: 'cancel',
          handler: (cubeId) => {
            if (!cubeId) { return; }

            const selectUsers = this.modalCtrl.create(SelectUsersPage);
            selectUsers.present();
            selectUsers.onDidDismiss(selection => {
              if (!selection) { return; }

              this.draftSvc.startCubeDraft(selection, cubeId).subscribe(draftId => {
                this.navCtrl.push(DraftPacksPage, { draftId });
              });
            });
          }
        }]
      }).present();

    });
  } //startCubeDraft

  public viewExistingDrafts() {
    this.navCtrl.push(DraftListPage);
  } //viewExistingDrafts

  public viewPastDrafts() {
    //
  } //viewPastDrafts

  public createNewCube() {
    this.alertCtrl.create({
      title: 'Name Your Cube',
      inputs: [{
        type: 'text',
        name: 'name',
        placeholder: 'Name your cube!'
      }],
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        cssClass: 'subdued'
      }, {
        text: 'Confirm',
        role: 'confirm',
        handler: (e) => {
          this.cubeSvc.createCube(e.name).subscribe(() => 'res')
        }
      }]
    }).present();
  } //createNewCube

} //MainPage
