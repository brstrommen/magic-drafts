import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';

import { DraftPacksPage } from '../';
import { DraftService } from '../../services/';


@Component({
  selector: 'page-draft-list',
  templateUrl: 'draft-list.html'
})
export class DraftListPage {
  public myDrafts: Observable<any>;

  constructor(
    public navCtrl: NavController,
    public draftSvc: DraftService
  ) {
    this.myDrafts = this.draftSvc.getMyDrafts();
  } //constructor

  public viewDraft(draftId) {
    this.navCtrl.push(DraftPacksPage, { draftId });
  }

} //DraftListPage
