import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { concat, indexOf, pull } from 'lodash';
import { Observable } from 'rxjs';

import { UserService } from '../../services/user.svc';


@Component({
  selector: 'page-select-users',
  templateUrl: 'select-users.html'
})
export class SelectUsersPage {
  public users: Observable<any>;
  public availableUsers: any[] = [];
  public selectedUsers: any[] = [];
  public maxSelected: number = 999;
  public currentUser: string;

  constructor(
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public userSvc: UserService
  ) {
    if (this.navParams.get('maxSelected')) {
      this.maxSelected = this.navParams.get('maxSelected');
    }

    if (this.navParams.get('userList')) {
      this.availableUsers = this.navParams.get('userList');
    } else {
      this.users = this.userSvc.getAllUsers();
    }

    if (this.navParams.get('preselected')) {
      this.selectedUsers = concat(this.navParams.get('preselected'));
    } else {
      this.selectedUsers = [ ];
    }

    this.userSvc.uid$.take(1).subscribe(uid => {
      if (this.navParams.get('omitCurrentUser') === true) {
        pull(this.availableUsers, uid);
      }
      this.currentUser = uid
    });
  }

  public preselected (userId): boolean {
    return (
      indexOf(this.navParams.get('preselected'), userId) !== -1
    );
  }

  public selectUser(e: { userId: string, selected: boolean }) {
    if ( e.selected ) {
      this.selectedUsers.push(e.userId);
    } else {
      pull(this.selectedUsers, e.userId);
    } // end if
  }

}
