Known Deficiencies:

Split Cards and Flip Cards
Are two separate entries in the DB, need to modify the display to link to the other half, and find some solution so they use a common card ID


Planned Features:

Insert mana symbols (DONE)
Export deck and cube lists for Cockatrice
Implement main/sideboard
Game invites/acceptance
Draft history


Nice-to-Haves:

Import assistant
Order cube contents
